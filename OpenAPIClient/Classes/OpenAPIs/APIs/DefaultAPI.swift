//
// DefaultAPI.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation

open class DefaultAPI {
    /**
     Autenticar password de usuario
     
     - parameter requestId: (header) Número de tracking de la solicitud 
     - parameter channelId: (header) Canal consumidor 
     - parameter userId: (header) Nombre de Usuario consumidor 
     - parameter appoptPartyPasswordValidInqPartyPasswordValidInqRqType: (body)  
     - parameter appName: (header) Nombre de la aplicación consumidora (optional)
     - parameter clientDt: (header) Fecha y hora en cliente consumidor (optional)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func authPassword(requestId: String, channelId: String, userId: String, appoptPartyPasswordValidInqPartyPasswordValidInqRqType: AppoptPartyPasswordValidInqPartyPasswordValidInqRqType, appName: String? = nil, clientDt: Date? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: AppoptPartyPasswordValidInqPartyPasswordValidInqRsType?, _ error: Error?) -> Void)) {
        authPasswordWithRequestBuilder(requestId: requestId, channelId: channelId, userId: userId, appoptPartyPasswordValidInqPartyPasswordValidInqRqType: appoptPartyPasswordValidInqPartyPasswordValidInqRqType, appName: appName, clientDt: clientDt).execute(apiResponseQueue) { result -> Void in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Autenticar password de usuario
     - POST /v1/users/passwords/auths
     - Operación para la autenticación del password del usuario en Entrust
     - parameter requestId: (header) Número de tracking de la solicitud 
     - parameter channelId: (header) Canal consumidor 
     - parameter userId: (header) Nombre de Usuario consumidor 
     - parameter appoptPartyPasswordValidInqPartyPasswordValidInqRqType: (body)  
     - parameter appName: (header) Nombre de la aplicación consumidora (optional)
     - parameter clientDt: (header) Fecha y hora en cliente consumidor (optional)
     - returns: RequestBuilder<AppoptPartyPasswordValidInqPartyPasswordValidInqRsType> 
     */
    open class func authPasswordWithRequestBuilder(requestId: String, channelId: String, userId: String, appoptPartyPasswordValidInqPartyPasswordValidInqRqType: AppoptPartyPasswordValidInqPartyPasswordValidInqRqType, appName: String? = nil, clientDt: Date? = nil) -> RequestBuilder<AppoptPartyPasswordValidInqPartyPasswordValidInqRsType> {
        let path = "/v1/users/passwords/auths"
        let URLString = OpenAPIClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: appoptPartyPasswordValidInqPartyPasswordValidInqRqType)

        let url = URLComponents(string: URLString)

        let nillableHeaders: [String: Any?] = [
            "requestId": requestId.encodeToJSON(),
            "channelId": channelId.encodeToJSON(),
            "userId": userId.encodeToJSON(),
            "appName": appName?.encodeToJSON(),
            "clientDt": clientDt?.encodeToJSON(),
        ]

        let headerParameters = APIHelper.rejectNilHeaders(nillableHeaders)

        let requestBuilder: RequestBuilder<AppoptPartyPasswordValidInqPartyPasswordValidInqRsType>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: (url?.string ?? URLString), parameters: parameters, headers: headerParameters)
    }

    /**
     Autenticar usuario mock
     
     - parameter requestId: (header) Número de tracking de la solicitud 
     - parameter channelId: (header) Canal consumidor 
     - parameter userId: (header) Nombre de Usuario consumidor 
     - parameter appName: (header) Nombre de la aplicación consumidora (optional)
     - parameter clientDt: (header) Fecha y hora en cliente consumidor (optional)
     - parameter userName: (query)  (optional)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func info(requestId: String, channelId: String, userId: String, appName: String? = nil, clientDt: Date? = nil, userName: String? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: AppoptPartyPasswordValidInqPartyPasswordValidInqRsType?, _ error: Error?) -> Void)) {
        infoWithRequestBuilder(requestId: requestId, channelId: channelId, userId: userId, appName: appName, clientDt: clientDt, userName: userName).execute(apiResponseQueue) { result -> Void in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Autenticar usuario mock
     - GET /v1/users/info
     - Operación para la autenticación del usuario mock
     - parameter requestId: (header) Número de tracking de la solicitud 
     - parameter channelId: (header) Canal consumidor 
     - parameter userId: (header) Nombre de Usuario consumidor 
     - parameter appName: (header) Nombre de la aplicación consumidora (optional)
     - parameter clientDt: (header) Fecha y hora en cliente consumidor (optional)
     - parameter userName: (query)  (optional)
     - returns: RequestBuilder<AppoptPartyPasswordValidInqPartyPasswordValidInqRsType> 
     */
    open class func infoWithRequestBuilder(requestId: String, channelId: String, userId: String, appName: String? = nil, clientDt: Date? = nil, userName: String? = nil) -> RequestBuilder<AppoptPartyPasswordValidInqPartyPasswordValidInqRsType> {
        let path = "/v1/users/info"
        let URLString = OpenAPIClientAPI.basePath + path
        let parameters: [String: Any]? = nil

        var url = URLComponents(string: URLString)
        url?.queryItems = APIHelper.mapValuesToQueryItems([
            "userName": userName?.encodeToJSON(),
        ])

        let nillableHeaders: [String: Any?] = [
            "requestId": requestId.encodeToJSON(),
            "channelId": channelId.encodeToJSON(),
            "userId": userId.encodeToJSON(),
            "appName": appName?.encodeToJSON(),
            "clientDt": clientDt?.encodeToJSON(),
        ]

        let headerParameters = APIHelper.rejectNilHeaders(nillableHeaders)

        let requestBuilder: RequestBuilder<AppoptPartyPasswordValidInqPartyPasswordValidInqRsType>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, headers: headerParameters)
    }

    /**
     V1UsersAffiliationsInfo_GET
     
     - parameter requestId: (header) Número de tracking de la solicitud 
     - parameter channelId: (header) Canal consumidor 
     - parameter userId: (header) Nombre de Usuario consumidor 
     - parameter appName: (header) Nombre de la aplicación consumidora (optional)
     - parameter clientDt: (header) Fecha y hora en cliente consumidor (optional)
     - parameter partyId: (query)  (optional)
     - parameter loginName: (query)  (optional)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func v1UsersAffiliationsInfoGET(requestId: String, channelId: String, userId: String, appName: String? = nil, clientDt: Date? = nil, partyId: String? = nil, loginName: String? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: CnsPartyAffiliationInfoInqRsType?, _ error: Error?) -> Void)) {
        v1UsersAffiliationsInfoGETWithRequestBuilder(requestId: requestId, channelId: channelId, userId: userId, appName: appName, clientDt: clientDt, partyId: partyId, loginName: loginName).execute(apiResponseQueue) { result -> Void in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     V1UsersAffiliationsInfo_GET
     - GET /v1/users/affiliations/info
     - parameter requestId: (header) Número de tracking de la solicitud 
     - parameter channelId: (header) Canal consumidor 
     - parameter userId: (header) Nombre de Usuario consumidor 
     - parameter appName: (header) Nombre de la aplicación consumidora (optional)
     - parameter clientDt: (header) Fecha y hora en cliente consumidor (optional)
     - parameter partyId: (query)  (optional)
     - parameter loginName: (query)  (optional)
     - returns: RequestBuilder<CnsPartyAffiliationInfoInqRsType> 
     */
    open class func v1UsersAffiliationsInfoGETWithRequestBuilder(requestId: String, channelId: String, userId: String, appName: String? = nil, clientDt: Date? = nil, partyId: String? = nil, loginName: String? = nil) -> RequestBuilder<CnsPartyAffiliationInfoInqRsType> {
        let path = "/v1/users/affiliations/info"
        let URLString = OpenAPIClientAPI.basePath + path
        let parameters: [String: Any]? = nil

        var url = URLComponents(string: URLString)
        url?.queryItems = APIHelper.mapValuesToQueryItems([
            "partyId": partyId?.encodeToJSON(),
            "loginName": loginName?.encodeToJSON(),
        ])

        let nillableHeaders: [String: Any?] = [
            "requestId": requestId.encodeToJSON(),
            "channelId": channelId.encodeToJSON(),
            "userId": userId.encodeToJSON(),
            "appName": appName?.encodeToJSON(),
            "clientDt": clientDt?.encodeToJSON(),
        ]

        let headerParameters = APIHelper.rejectNilHeaders(nillableHeaders)

        let requestBuilder: RequestBuilder<CnsPartyAffiliationInfoInqRsType>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, headers: headerParameters)
    }

    /**
     V1UsersOtpsAuths_POST
     
     - parameter requestId: (header) Número de tracking de la solicitud 
     - parameter channelId: (header) Canal consumidor 
     - parameter userId: (header) Nombre de Usuario consumidor 
     - parameter autenticarOTP: (body)  
     - parameter appName: (header) Nombre de la aplicación consumidora (optional)
     - parameter clientDt: (header) Fecha y hora en cliente consumidor (optional)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func v1UsersOtpsAuthsPOST(requestId: String, channelId: String, userId: String, autenticarOTP: AutenticarOTP, appName: String? = nil, clientDt: Date? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: AutenticarOTPResponse?, _ error: Error?) -> Void)) {
        v1UsersOtpsAuthsPOSTWithRequestBuilder(requestId: requestId, channelId: channelId, userId: userId, autenticarOTP: autenticarOTP, appName: appName, clientDt: clientDt).execute(apiResponseQueue) { result -> Void in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     V1UsersOtpsAuths_POST
     - POST /v1/users/otps/auths
     - parameter requestId: (header) Número de tracking de la solicitud 
     - parameter channelId: (header) Canal consumidor 
     - parameter userId: (header) Nombre de Usuario consumidor 
     - parameter autenticarOTP: (body)  
     - parameter appName: (header) Nombre de la aplicación consumidora (optional)
     - parameter clientDt: (header) Fecha y hora en cliente consumidor (optional)
     - returns: RequestBuilder<AutenticarOTPResponse> 
     */
    open class func v1UsersOtpsAuthsPOSTWithRequestBuilder(requestId: String, channelId: String, userId: String, autenticarOTP: AutenticarOTP, appName: String? = nil, clientDt: Date? = nil) -> RequestBuilder<AutenticarOTPResponse> {
        let path = "/v1/users/otps/auths"
        let URLString = OpenAPIClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: autenticarOTP)

        let url = URLComponents(string: URLString)

        let nillableHeaders: [String: Any?] = [
            "requestId": requestId.encodeToJSON(),
            "channelId": channelId.encodeToJSON(),
            "userId": userId.encodeToJSON(),
            "appName": appName?.encodeToJSON(),
            "clientDt": clientDt?.encodeToJSON(),
        ]

        let headerParameters = APIHelper.rejectNilHeaders(nillableHeaders)

        let requestBuilder: RequestBuilder<AutenticarOTPResponse>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: (url?.string ?? URLString), parameters: parameters, headers: headerParameters)
    }

    /**
     V1UsersOtpsDeliveries_POST
     
     - parameter requestId: (header) Número de tracking de la solicitud 
     - parameter channelId: (header) Canal consumidor 
     - parameter userId: (header) Nombre de Usuario consumidor 
     - parameter entregarOtpBus: (body)  
     - parameter appName: (header) Nombre de la aplicación consumidora (optional)
     - parameter clientDt: (header) Fecha y hora en cliente consumidor (optional)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func v1UsersOtpsDeliveriesPOST(requestId: String, channelId: String, userId: String, entregarOtpBus: EntregarOtpBus, appName: String? = nil, clientDt: Date? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: EntregarOtpBusResponse?, _ error: Error?) -> Void)) {
        v1UsersOtpsDeliveriesPOSTWithRequestBuilder(requestId: requestId, channelId: channelId, userId: userId, entregarOtpBus: entregarOtpBus, appName: appName, clientDt: clientDt).execute(apiResponseQueue) { result -> Void in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     V1UsersOtpsDeliveries_POST
     - POST /v1/users/otps/deliveries
     - parameter requestId: (header) Número de tracking de la solicitud 
     - parameter channelId: (header) Canal consumidor 
     - parameter userId: (header) Nombre de Usuario consumidor 
     - parameter entregarOtpBus: (body)  
     - parameter appName: (header) Nombre de la aplicación consumidora (optional)
     - parameter clientDt: (header) Fecha y hora en cliente consumidor (optional)
     - returns: RequestBuilder<EntregarOtpBusResponse> 
     */
    open class func v1UsersOtpsDeliveriesPOSTWithRequestBuilder(requestId: String, channelId: String, userId: String, entregarOtpBus: EntregarOtpBus, appName: String? = nil, clientDt: Date? = nil) -> RequestBuilder<EntregarOtpBusResponse> {
        let path = "/v1/users/otps/deliveries"
        let URLString = OpenAPIClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: entregarOtpBus)

        let url = URLComponents(string: URLString)

        let nillableHeaders: [String: Any?] = [
            "requestId": requestId.encodeToJSON(),
            "channelId": channelId.encodeToJSON(),
            "userId": userId.encodeToJSON(),
            "appName": appName?.encodeToJSON(),
            "clientDt": clientDt?.encodeToJSON(),
        ]

        let headerParameters = APIHelper.rejectNilHeaders(nillableHeaders)

        let requestBuilder: RequestBuilder<EntregarOtpBusResponse>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: (url?.string ?? URLString), parameters: parameters, headers: headerParameters)
    }

    /**
     V1UsersTokensAuths_POST
     
     - parameter requestId: (header) Número de tracking de la solicitud 
     - parameter channelId: (header) Canal consumidor 
     - parameter userId: (header) Nombre de Usuario consumidor 
     - parameter autenticarToken: (body)  
     - parameter appName: (header) Nombre de la aplicación consumidora (optional)
     - parameter clientDt: (header) Fecha y hora en cliente consumidor (optional)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func v1UsersTokensAuthsPOST(requestId: String, channelId: String, userId: String, autenticarToken: AutenticarToken, appName: String? = nil, clientDt: Date? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: AutenticarTokenResponse?, _ error: Error?) -> Void)) {
        v1UsersTokensAuthsPOSTWithRequestBuilder(requestId: requestId, channelId: channelId, userId: userId, autenticarToken: autenticarToken, appName: appName, clientDt: clientDt).execute(apiResponseQueue) { result -> Void in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     V1UsersTokensAuths_POST
     - POST /v1/users/tokens/auths
     - parameter requestId: (header) Número de tracking de la solicitud 
     - parameter channelId: (header) Canal consumidor 
     - parameter userId: (header) Nombre de Usuario consumidor 
     - parameter autenticarToken: (body)  
     - parameter appName: (header) Nombre de la aplicación consumidora (optional)
     - parameter clientDt: (header) Fecha y hora en cliente consumidor (optional)
     - returns: RequestBuilder<AutenticarTokenResponse> 
     */
    open class func v1UsersTokensAuthsPOSTWithRequestBuilder(requestId: String, channelId: String, userId: String, autenticarToken: AutenticarToken, appName: String? = nil, clientDt: Date? = nil) -> RequestBuilder<AutenticarTokenResponse> {
        let path = "/v1/users/tokens/auths"
        let URLString = OpenAPIClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: autenticarToken)

        let url = URLComponents(string: URLString)

        let nillableHeaders: [String: Any?] = [
            "requestId": requestId.encodeToJSON(),
            "channelId": channelId.encodeToJSON(),
            "userId": userId.encodeToJSON(),
            "appName": appName?.encodeToJSON(),
            "clientDt": clientDt?.encodeToJSON(),
        ]

        let headerParameters = APIHelper.rejectNilHeaders(nillableHeaders)

        let requestBuilder: RequestBuilder<AutenticarTokenResponse>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: (url?.string ?? URLString), parameters: parameters, headers: headerParameters)
    }

}
