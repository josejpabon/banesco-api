//
// AppoptPartyPasswordValidInquserSelect.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation

public struct AppoptPartyPasswordValidInquserSelect: Codable {

    public enum CanalProcedencia: String, Codable, CaseIterable {
        case mobileBanking = "MobileBanking"
        case internetBanking = "InternetBanking"
    }
    /** Canal Origen de la solicitud.  * &#x60;MobileBanking&#x60; - Banca Móvil * &#x60;InternetBanking&#x60; - Banca Web  */
    public var canalProcedencia: CanalProcedencia?
    /** Grupo de Entrust IDG en el cual está definido el usuario. Por defecto es &#x60;Banesco&#x60;.  */
    public var grupo: String? = "Banesco"
    /** Username del usuario en Entrust IDG. */
    public var idUsuario: String?

    public init(canalProcedencia: CanalProcedencia? = nil, grupo: String? = "Banesco", idUsuario: String? = nil) {
        self.canalProcedencia = canalProcedencia
        self.grupo = grupo
        self.idUsuario = idUsuario
    }

}
