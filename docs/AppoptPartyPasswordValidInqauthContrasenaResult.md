# AppoptPartyPasswordValidInqauthContrasenaResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**affiliationStatus** | **String** |  | [optional] 
**authenticator** | **String** |  | [optional] 
**preferredAuthStatus** | **String** |  | [optional] 
**serial** | **String** |  | [optional] 
**success** | [**AppoptPartyPasswordValidInqsuccess**](AppoptPartyPasswordValidInqsuccess.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


