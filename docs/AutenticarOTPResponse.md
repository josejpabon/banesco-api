# AutenticarOTPResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**autenticarOTPResult** | [**ResultStatusType**](ResultStatusType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


