# AutenticarToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**autenticarTokenIn** | [**AutenticarTokenIn**](AutenticarTokenIn.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


