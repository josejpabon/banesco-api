# CnsLoginIdent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loginName** | **String** | Nombre de usuario | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


