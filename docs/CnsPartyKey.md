# CnsPartyKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partyId** | **String** | Identificador unico del cliente | [optional] 
**loginIdent** | [**CnsLoginIdent**](CnsLoginIdent.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


