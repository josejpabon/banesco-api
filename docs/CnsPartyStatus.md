# CnsPartyStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**affiliationStatus** | **String** | Estatus Usuario | [optional] 
**preferredAuthStatus** | **String** | Estatus afiliacion | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


