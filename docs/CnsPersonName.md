# CnsPersonName

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **String** | Primer nombre | [optional] 
**fullName** | **String** | Nombre completo | [optional] 
**lastName** | **String** | Primer Apellido | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


