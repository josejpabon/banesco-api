# DefaultAPI

All URIs are relative to *https://www.example.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authPassword**](DefaultAPI.md#authpassword) | **POST** /v1/users/passwords/auths | Autenticar password de usuario
[**info**](DefaultAPI.md#info) | **GET** /v1/users/info | Autenticar usuario mock
[**v1UsersAffiliationsInfoGET**](DefaultAPI.md#v1usersaffiliationsinfoget) | **GET** /v1/users/affiliations/info | V1UsersAffiliationsInfo_GET
[**v1UsersOtpsAuthsPOST**](DefaultAPI.md#v1usersotpsauthspost) | **POST** /v1/users/otps/auths | V1UsersOtpsAuths_POST
[**v1UsersOtpsDeliveriesPOST**](DefaultAPI.md#v1usersotpsdeliveriespost) | **POST** /v1/users/otps/deliveries | V1UsersOtpsDeliveries_POST
[**v1UsersTokensAuthsPOST**](DefaultAPI.md#v1userstokensauthspost) | **POST** /v1/users/tokens/auths | V1UsersTokensAuths_POST


# **authPassword**
```swift
    open class func authPassword(requestId: String, channelId: String, userId: String, appoptPartyPasswordValidInqPartyPasswordValidInqRqType: AppoptPartyPasswordValidInqPartyPasswordValidInqRqType, appName: String? = nil, clientDt: Date? = nil, completion: @escaping (_ data: AppoptPartyPasswordValidInqPartyPasswordValidInqRsType?, _ error: Error?) -> Void)
```

Autenticar password de usuario

Operación para la autenticación del password del usuario en Entrust

### Example 
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let requestId = "requestId_example" // String | Número de tracking de la solicitud
let channelId = "channelId_example" // String | Canal consumidor
let userId = "userId_example" // String | Nombre de Usuario consumidor
let appoptPartyPasswordValidInqPartyPasswordValidInqRqType = appoptPartyPasswordValidInqPartyPasswordValidInqRqType(authContrasena: appoptPartyPasswordValidInqauthContrasena(authContrasenaIn: appoptPartyPasswordValidInqauthContrasenaIn(pass: "pass_example", userSelect: appoptPartyPasswordValidInquserSelect(canalProcedencia: "canalProcedencia_example", grupo: "grupo_example", idUsuario: "idUsuario_example")))) // AppoptPartyPasswordValidInqPartyPasswordValidInqRqType | 
let appName = "appName_example" // String | Nombre de la aplicación consumidora (optional)
let clientDt = Date() // Date | Fecha y hora en cliente consumidor (optional)

// Autenticar password de usuario
DefaultAPI.authPassword(requestId: requestId, channelId: channelId, userId: userId, appoptPartyPasswordValidInqPartyPasswordValidInqRqType: appoptPartyPasswordValidInqPartyPasswordValidInqRqType, appName: appName, clientDt: clientDt) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | **String** | Número de tracking de la solicitud | 
 **channelId** | **String** | Canal consumidor | 
 **userId** | **String** | Nombre de Usuario consumidor | 
 **appoptPartyPasswordValidInqPartyPasswordValidInqRqType** | [**AppoptPartyPasswordValidInqPartyPasswordValidInqRqType**](AppoptPartyPasswordValidInqPartyPasswordValidInqRqType.md) |  | 
 **appName** | **String** | Nombre de la aplicación consumidora | [optional] 
 **clientDt** | **Date** | Fecha y hora en cliente consumidor | [optional] 

### Return type

[**AppoptPartyPasswordValidInqPartyPasswordValidInqRsType**](AppoptPartyPasswordValidInqPartyPasswordValidInqRsType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **info**
```swift
    open class func info(requestId: String, channelId: String, userId: String, appName: String? = nil, clientDt: Date? = nil, userName: String? = nil, completion: @escaping (_ data: AppoptPartyPasswordValidInqPartyPasswordValidInqRsType?, _ error: Error?) -> Void)
```

Autenticar usuario mock

Operación para la autenticación del usuario mock

### Example 
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let requestId = "requestId_example" // String | Número de tracking de la solicitud
let channelId = "channelId_example" // String | Canal consumidor
let userId = "userId_example" // String | Nombre de Usuario consumidor
let appName = "appName_example" // String | Nombre de la aplicación consumidora (optional)
let clientDt = Date() // Date | Fecha y hora en cliente consumidor (optional)
let userName = "userName_example" // String |  (optional)

// Autenticar usuario mock
DefaultAPI.info(requestId: requestId, channelId: channelId, userId: userId, appName: appName, clientDt: clientDt, userName: userName) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | **String** | Número de tracking de la solicitud | 
 **channelId** | **String** | Canal consumidor | 
 **userId** | **String** | Nombre de Usuario consumidor | 
 **appName** | **String** | Nombre de la aplicación consumidora | [optional] 
 **clientDt** | **Date** | Fecha y hora en cliente consumidor | [optional] 
 **userName** | **String** |  | [optional] 

### Return type

[**AppoptPartyPasswordValidInqPartyPasswordValidInqRsType**](AppoptPartyPasswordValidInqPartyPasswordValidInqRsType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1UsersAffiliationsInfoGET**
```swift
    open class func v1UsersAffiliationsInfoGET(requestId: String, channelId: String, userId: String, appName: String? = nil, clientDt: Date? = nil, partyId: String? = nil, loginName: String? = nil, completion: @escaping (_ data: CnsPartyAffiliationInfoInqRsType?, _ error: Error?) -> Void)
```

V1UsersAffiliationsInfo_GET

### Example 
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let requestId = "requestId_example" // String | Número de tracking de la solicitud
let channelId = "channelId_example" // String | Canal consumidor
let userId = "userId_example" // String | Nombre de Usuario consumidor
let appName = "appName_example" // String | Nombre de la aplicación consumidora (optional)
let clientDt = Date() // Date | Fecha y hora en cliente consumidor (optional)
let partyId = "partyId_example" // String |  (optional)
let loginName = "loginName_example" // String |  (optional)

// V1UsersAffiliationsInfo_GET
DefaultAPI.v1UsersAffiliationsInfoGET(requestId: requestId, channelId: channelId, userId: userId, appName: appName, clientDt: clientDt, partyId: partyId, loginName: loginName) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | **String** | Número de tracking de la solicitud | 
 **channelId** | **String** | Canal consumidor | 
 **userId** | **String** | Nombre de Usuario consumidor | 
 **appName** | **String** | Nombre de la aplicación consumidora | [optional] 
 **clientDt** | **Date** | Fecha y hora en cliente consumidor | [optional] 
 **partyId** | **String** |  | [optional] 
 **loginName** | **String** |  | [optional] 

### Return type

[**CnsPartyAffiliationInfoInqRsType**](CnsPartyAffiliationInfoInqRsType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1UsersOtpsAuthsPOST**
```swift
    open class func v1UsersOtpsAuthsPOST(requestId: String, channelId: String, userId: String, autenticarOTP: AutenticarOTP, appName: String? = nil, clientDt: Date? = nil, completion: @escaping (_ data: AutenticarOTPResponse?, _ error: Error?) -> Void)
```

V1UsersOtpsAuths_POST

### Example 
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let requestId = "requestId_example" // String | Número de tracking de la solicitud
let channelId = "channelId_example" // String | Canal consumidor
let userId = "userId_example" // String | Nombre de Usuario consumidor
let autenticarOTP = autenticarOTP(autenticarOTPIn: autenticarOTPIn(canalProcedencia: "canalProcedencia_example", idUsuario: "idUsuario_example", grupo: "grupo_example", otp: "otp_example")) // AutenticarOTP | 
let appName = "appName_example" // String | Nombre de la aplicación consumidora (optional)
let clientDt = Date() // Date | Fecha y hora en cliente consumidor (optional)

// V1UsersOtpsAuths_POST
DefaultAPI.v1UsersOtpsAuthsPOST(requestId: requestId, channelId: channelId, userId: userId, autenticarOTP: autenticarOTP, appName: appName, clientDt: clientDt) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | **String** | Número de tracking de la solicitud | 
 **channelId** | **String** | Canal consumidor | 
 **userId** | **String** | Nombre de Usuario consumidor | 
 **autenticarOTP** | [**AutenticarOTP**](AutenticarOTP.md) |  | 
 **appName** | **String** | Nombre de la aplicación consumidora | [optional] 
 **clientDt** | **Date** | Fecha y hora en cliente consumidor | [optional] 

### Return type

[**AutenticarOTPResponse**](AutenticarOTPResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1UsersOtpsDeliveriesPOST**
```swift
    open class func v1UsersOtpsDeliveriesPOST(requestId: String, channelId: String, userId: String, entregarOtpBus: EntregarOtpBus, appName: String? = nil, clientDt: Date? = nil, completion: @escaping (_ data: EntregarOtpBusResponse?, _ error: Error?) -> Void)
```

V1UsersOtpsDeliveries_POST

### Example 
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let requestId = "requestId_example" // String | Número de tracking de la solicitud
let channelId = "channelId_example" // String | Canal consumidor
let userId = "userId_example" // String | Nombre de Usuario consumidor
let entregarOtpBus = entregarOtpBus(entregarOtpBusIn: EntregarOtpBusQueryType(userSelect: UsuarioYGrupoType(canalProcedencia: "canalProcedencia_example", idUsuario: "idUsuario_example", grupo: "grupo_example"), direccion: "direccion_example")) // EntregarOtpBus | 
let appName = "appName_example" // String | Nombre de la aplicación consumidora (optional)
let clientDt = Date() // Date | Fecha y hora en cliente consumidor (optional)

// V1UsersOtpsDeliveries_POST
DefaultAPI.v1UsersOtpsDeliveriesPOST(requestId: requestId, channelId: channelId, userId: userId, entregarOtpBus: entregarOtpBus, appName: appName, clientDt: clientDt) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | **String** | Número de tracking de la solicitud | 
 **channelId** | **String** | Canal consumidor | 
 **userId** | **String** | Nombre de Usuario consumidor | 
 **entregarOtpBus** | [**EntregarOtpBus**](EntregarOtpBus.md) |  | 
 **appName** | **String** | Nombre de la aplicación consumidora | [optional] 
 **clientDt** | **Date** | Fecha y hora en cliente consumidor | [optional] 

### Return type

[**EntregarOtpBusResponse**](EntregarOtpBusResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **v1UsersTokensAuthsPOST**
```swift
    open class func v1UsersTokensAuthsPOST(requestId: String, channelId: String, userId: String, autenticarToken: AutenticarToken, appName: String? = nil, clientDt: Date? = nil, completion: @escaping (_ data: AutenticarTokenResponse?, _ error: Error?) -> Void)
```

V1UsersTokensAuths_POST

### Example 
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let requestId = "requestId_example" // String | Número de tracking de la solicitud
let channelId = "channelId_example" // String | Canal consumidor
let userId = "userId_example" // String | Nombre de Usuario consumidor
let autenticarToken = autenticarToken(autenticarTokenIn: autenticarTokenIn(tokenSelect: TokenSelectType(canalProcedencia: "canalProcedencia_example", soft: false, idUsuario: "idUsuario_example", grupo: "grupo_example", serialToken: "serialToken_example"), valorToken: "valorToken_example")) // AutenticarToken | 
let appName = "appName_example" // String | Nombre de la aplicación consumidora (optional)
let clientDt = Date() // Date | Fecha y hora en cliente consumidor (optional)

// V1UsersTokensAuths_POST
DefaultAPI.v1UsersTokensAuthsPOST(requestId: requestId, channelId: channelId, userId: userId, autenticarToken: autenticarToken, appName: appName, clientDt: clientDt) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | **String** | Número de tracking de la solicitud | 
 **channelId** | **String** | Canal consumidor | 
 **userId** | **String** | Nombre de Usuario consumidor | 
 **autenticarToken** | [**AutenticarToken**](AutenticarToken.md) |  | 
 **appName** | **String** | Nombre de la aplicación consumidora | [optional] 
 **clientDt** | **Date** | Fecha y hora en cliente consumidor | [optional] 

### Return type

[**AutenticarTokenResponse**](AutenticarTokenResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

