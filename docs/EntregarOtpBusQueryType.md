# EntregarOtpBusQueryType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userSelect** | [**UsuarioYGrupoType**](UsuarioYGrupoType.md) |  | [optional] 
**direccion** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


