# EoStatusStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**statusCode** | **String** | Status Code | [optional] 
**statusDesc** | **String** | Status Description | [optional] 
**additionalStatus** | [**EoStatusAdditionalStatus**](EoStatusAdditionalStatus.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


