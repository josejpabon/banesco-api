# ResultStatusType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ok** | **Bool** | Flag para indicar el resultado de la operación. Éxito &#x3D; &#x60;true&#x60; ; Error &#x3D; &#x60;false&#x60; | 
**mensaje** | **String** | Mensaje de respuesta. Si ocurre éxito, retorna vacío. Si ocurre error, retorna la descripción del error | 
**codigo** | **String** | Código de respuesta. Si ocurre éxito, retorna \&quot;0000\&quot;. Si ocurre error, retorna el número del código de error. Por ejemplo, \&quot;5203113\&quot;. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


