# TokenSelectType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**canalProcedencia** | **String** | Canal Origen de la solicitud.  * &#x60;MobileBanking&#x60; - Banca Móvil * &#x60;InternetBanking&#x60; - Banca Web  | [optional] 
**soft** | **Bool** |  | 
**idUsuario** | **String** | Username del usuario en Entrust IDG. | [optional] 
**grupo** | **String** | Grupo de Entrust IDG en el cual está definido el usuario. Por defecto es &#x60;Banesco&#x60;.  | [optional] [default to "Banesco"]
**serialToken** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


